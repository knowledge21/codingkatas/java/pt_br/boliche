Jogo de Boliche
===============

Crie uma classe que faça o cálculo da pontuação de um jogo de boliche completo segundo as seguintes regras:

- Usualmente o jogo consiste de 10 rodadas de até 2 bolas
- Em cada rodada pode-se derrubar até 10 pinos
- A pontuação de cada rodada é igual à soma dos pinos derrubados pelas 2 bolas + os seguintes casos especiais:
>- Caso os 10 pinos sejam derrubados na segunda bola é marcado um **FRAME**. Neste caso se soma à pontuação da rodada os pinos derrubados na bola seguinte;
>>- Caso a última rodada seja um *SPARE*, o jogador tem uma bola extra;
>- Caso os 10 pinos sejam derrubados na primeira bola da rodada é marcado um **STRIKE**. Neste caso se soma à pontuação da rodada os pinos derrubados nas duas bolas seguintes;
>>- Caso a última rodada seja um *STRIKE*, o jogador tem duas bolas extras
- Ao final das 10 rodadas, soma-se a pontuação de todas as rodadas para se chegar à pontuação final
- A pontuação máxima é **300**